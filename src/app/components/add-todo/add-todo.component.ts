import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.css']
})
export class AddTodoComponent implements OnInit {

  @Output() addNewTodo: EventEmitter<any> = new EventEmitter();


  constructor() { }


  ngOnInit() {
  }

  addTodo(newtask: HTMLInputElement): boolean {

    if (newtask.value !== '') {

      const todo = {
        title: newtask.value,
        completed: false
      };

      this.addNewTodo.emit(todo);


    }
    return false;
  }

}

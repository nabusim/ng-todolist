import { Component, OnInit } from '@angular/core';
import { Todo } from '../../models/Todo';
import { TodoService } from '../../services/todo.service';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {

  todoList: Todo[];

  constructor(private todoService: TodoService) { }

  ngOnInit() {
    this.todoService.getTodos().subscribe(todos => {
      this.todoList = todos;
    });
  }

  deleteTodo(todo: Todo) {
    // Delete in UI
    this.todoList = this.todoList.filter(t => t.id !== todo.id);
    // Delete on Server
    this.todoService.deleteTodo(todo).subscribe();
  }

  addNewTodo(todo: Todo) {
    this.todoService.addTodo(todo).subscribe(todo => {
      this.todoList.push(todo);
    });
  }


}
